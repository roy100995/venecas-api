from rest_framework import serializers
from photos.models import Image

class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'src', 'name', 'description')

    def to_representation(self, instance):
        data = super().to_representation(instance)
        return data