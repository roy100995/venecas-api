from django.shortcuts import render, get_object_or_404
from .models import Image

def photos(request):
    images = Image.objects.all()
    return render(request, 'photos.html', {'images': images})

def photo(request, id_photo):
    image = get_object_or_404(Image, pk=id_photo)
    return render(request, 'photo.html', {'image': image})

def home(request): 
    return render(request, 'home.html')