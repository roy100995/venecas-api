from django.db import models

class Image(models.Model):
    src = models.ImageField(upload_to='images/')
    name = models.CharField(max_length=100)
    description = models.TextField()
    